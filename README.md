CONTENTS OF THIS FILE
---------------------

* Introduction
* Recommended modules
* Installation
* Configuration
* Troubleshooting
* Maintainers

INTRODUCTION
------------


The Developer Entity Browser module is designed to enhance developer efficiency by providing a unified interface for comprehensive oversight of all content entities and entity bundles. It enables developers to seamlessly navigate through entities, aggregate field names, and review configuration settings—all from a single, intuitive dashboard.

The primary use case for this module is:

- Help to give you and overview of a content entity fields.
- Useful when Field UI is not turned on.
- Or for custom entities that might not have a UI.

To view the report, simply go to the Reports page and choose Developer Entity Browser or go to
`/admin/reports/dev-entity-browser`.

RECOMMENDED MODULES
-------------------

* No extra module is required.

INSTALLATION
------------

* Install as usual, see
  https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 for further
  information.

CONFIGURATION
-------------

* Permissions: Set "View Dev Entity Browser" permission for the desired roles.

MAINTAINERS
-----------
Current maintainers :

* Matt Burke (https://www.drupal.org/u/mattyb)

## SUPPORTING ORGANISATIONS

https://www.adaptive.co.uk
