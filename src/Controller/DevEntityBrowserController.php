<?php

declare(strict_types=1);

namespace Drupal\dev_entity_browser\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Developer Entity Browser routes.
 */
final class DevEntityBrowserController extends ControllerBase {

  /**
   * The controller constructor.
   */
  public function __construct(
    private readonly EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    private readonly EntityFieldManagerInterface $entityFieldManager,
    EntityTypeManagerInterface $entityTypeManager,
  ) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Collect data about the entities, bundles, and fields.
   */
  public function __invoke(): array {
    // Collection Array.
    $table_of_contents = [];
    // Content Entity Types.
    $content_entity_types = [];
    // Collect Field Maps.
    $field_map = $this->collectFieldMaps();
    // Collect Entity Definitions.
    $entity_type_definitions = $this->collectEntityDefinitions();
    foreach ($entity_type_definitions as $key => $definition) {
      if ($definition instanceof ContentEntityType) {
        $content_entity_types[$key] = $definition;
      }
    }

    foreach ($content_entity_types as $entity_type_id => $entity_type) {
      $table_of_contents[$entity_type_id] = [
        'label' => self::convertLabelToString($entity_type->getLabel()),
        'machine_name' => $entity_type_id,
        'class_name' => $entity_type->getClass(),
        'bundles' => [],
      ];
      switch ($entity_type->id()) {
        // Remove unwanted Entities.
        case 'block_content':
        case 'webform_submission':
          break;

        default:
          $bundles = $this->entityTypeBundleInfo
            ->getBundleInfo($entity_type->id());
          foreach ($bundles as $bundle_id => $bundle) {
            $build_build = [
              'machine_name' => $bundle_id,
              'label' => self::convertLabelToString($bundle['label']),
              'fields' => [],
              'fieldMap' => [],
            ];
            if (isset($field_map[$entity_type->id()])) {
              foreach ($field_map[$entity_type->id()] as $field_name => $field_info) {
                foreach ($field_info['bundles'] as $field_bundle_key => $field_bundle_name) {
                  $build_build['fieldMap'][$field_bundle_key][$field_name] = [
                    'name' => $field_name,
                    'type' => $field_info['type'],
                  ];
                }
              }
            }
            // Get Bundle specific fields.
            $all_bundle_fields = $this->entityFieldManager
              ->getFieldDefinitions($entity_type_id, $bundle_id);
            foreach ($all_bundle_fields as $bundle_field_key => $bundle_field_info) {
              $field_build = [];
              $getSettings = ($bundle_field_info->getItemDefinition()
                ->getSettings());
              $field_build['description'] = $this->convertLabelToString($bundle_field_info->getDescription());
              // Allowed fields.
              if (isset($getSettings['allowed_values'])) {
                $field_build['allowed_values'] = [];
                foreach ($getSettings['allowed_values'] as $allowed_value_key => $allowed_value) {
                  $field_build['allowed_values'][$allowed_value_key] = $allowed_value;
                }
              }
              $extraInfo = $this->exploreType($entity_type_id, $bundle_id, $bundle_field_info, $bundle_field_key);
              $field_build['info'] = $extraInfo;
              $field_build['label_label'] = $this->convertLabelToString($bundle_field_info->getLabel());
              $field_build['label_key'] = $this->convertLabelToString($bundle_field_key);
              $field_build['cardinality'] = 1;
              if (method_exists($bundle_field_info, 'getCardinality')) {
                $field_build['cardinality'] = ($bundle_field_info->getCardinality());
              }
              $build_build['fields'][$bundle_field_key] = $field_build;
              $table_of_contents[$entity_type_id]['bundles'][$bundle_id] = $build_build;
            }
          }
          break;
      }
    }
    // Sort the table of contents by the label rather than machine id.
    $table_of_contentsOrder = [];
    foreach ($table_of_contents as $tocKey => $toc) {
      $table_of_contentsOrder[$toc['label'] . $tocKey] = $toc;
    }
    // Perform the sort.
    ksort($table_of_contentsOrder, SORT_STRING);
    // Add to render array.
    $build['dev_entity_browser_report'] = [
      '#type' => 'dev_entity_browser_report',
      '#theme' => 'dev_entity_browser_report',
      '#report' => $table_of_contentsOrder,
    ];

    return $build;
  }

  /**
   * Gets a lightweight map of fields across bundles.
   */
  private function collectFieldMaps(): array {
    return $this->entityFieldManager->getFieldMap();
  }

  /**
   * Get a list of entity definitions.
   */
  private function collectEntityDefinitions() {
    return $this->entityTypeManager->getDefinitions();
  }

  /**
   * Convert a label to a string.
   *
   * @param mixed $label
   *   e.g. $entity_type->getLabel()|$bundle['label'].
   *
   * @return string
   *   The label as a string.
   */
  private function convertLabelToString(mixed $label): string {
    $outputLabel = '';
    if ($label) {
      if (is_object($label) and method_exists($label, '__toString')) {
        $outputLabel = $label->__toString();
      }
      elseif (is_string($label)) {
        $outputLabel = $label;
      }
    }
    return $outputLabel;
  }

  /**
   * Explore the field by type and return a field report.
   *
   * @param int $entity_id
   *   The entity id.
   * @param int|string $bundle_id
   *   mixed The bundle id.
   * @param mixed $bundle_field_info
   *   mixed  Field Definition.
   * @param mixed $bundle_field_key
   *   mixed  Field Key.
   *
   * @return array
   *   Return the report for the field.
   */
  private function exploreType($entity_id, $bundle_id, $bundle_field_info, $bundle_field_key): array {
    // dsm( $bundle_field_info->getType());
    $report = [
      'type' => $bundle_field_info->getType(),
    ];
    switch ($bundle_field_info->getType()) {
      case 'file':
      case 'image':
      case 'entity_reference':
      case 'entity_reference_revisions':
      case 'list_string':
      case 'list_integer':
      case 'list_float':
        /***
         * @var \Drupal\field\Entity\FieldConfig $fieldConfig
         */
        $fieldConfig = FieldConfig::loadByName($entity_id, $bundle_id, $bundle_field_key);
        if ($fieldConfig != NULL) {
          $settings = ($fieldConfig->getSettings());
          $this->getFieldSettingsReport($settings, $report, $bundle_field_key);
        }
        else {
          if (is_object($bundle_field_info)) {
            $settings = $bundle_field_info->getSettings();
            $this->getFieldSettingsReport($settings, $report, $bundle_field_key);
          }
        }
    }
    return $report;
  }

  /**
   * Build a report based on the field settings.
   *
   * @param array $settings
   *   Field Settings.
   * @param array $report
   *   Ongoing report array.
   * @param string $field
   *   Field Name.
   *
   * @return void
   *   Void. $report is passed by reference.
   */
  private function getFieldSettingsReport(array $settings, array &$report, string $field): void {
    foreach ($settings as $key => $value) {
      $report['settings'][$key] = ['id' => $key, 'field' => $field];
      if (is_string($value)) {
        $report['settings'][$key]['value'] = [str_replace('_', ' ', $value)];
      }
      else {
        if (is_array($value)) {
          if (isset($value['target_bundles']) && is_array($value['target_bundles']) && count($value['target_bundles'])) {
            $report['settings'][$key]['target_bundles'] = $value['target_bundles'];
          }
        }
      }
    }
  }

}
